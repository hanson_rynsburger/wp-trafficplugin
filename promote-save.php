<?php
    require('../../../wp-blog-header.php');
    header("HTTP/1.1 200 OK");

    $id = $_POST['id'];
    $post = get_post($id);

    save_traffic_meta($id, $post);

    function save_traffic_meta( $post_id, $post ) {
      if (!isset($_POST['trf_traffic_nonce']) || !wp_verify_nonce($_POST['trf_traffic_nonce'], 'trf_'.$post->ID))
        return $post_id;

      $post_type = get_post_type_object($post->post_type);

      if (!current_user_can($post_type->cap->edit_post, $post_id))
        return $post_id;

      if ($_POST['keyword'] === 'true') {
        save_or_update_meta($post_id, 'keyword1', 'trf_keyword1');
        save_or_update_meta($post_id, 'keyword2', 'trf_keyword2');
        save_or_update_meta($post_id, 'keyword3', 'trf_keyword3');
      } else {
        save_or_update_meta($post_id, 'get_traffic', 'trf_get_traffic');
      }
    }

    // -----------------------------

    if ($_POST['keyword'] === 'true') return;

    $get_traffic  = get_post_meta($id, 'trf_get_traffic', true);

    if ($get_traffic != '1') return;

    $facebookReady  = !empty(get_post_meta(111111113, 'trfFbAccessToken', TRUE));
    $twitterReady   = !empty(get_post_meta(111111113, 'trfTwTokenSecret', TRUE));
    $redditReady    = !empty(get_post_meta(111111113, 'trfRdRefreshToken', TRUE));

    $keyword1       = get_post_meta($id, 'trf_keyword1', true);
    $keyword2       = get_post_meta($id, 'trf_keyword2', true);
    $keyword3       = get_post_meta($id, 'trf_keyword3', true);

    if ($get_traffic == 1) {
      if ($facebookReady) {
        $keywordErrors = array();
        if (!empty($keyword1)) {
          $err = trfProcesskeyword($keyword1);
          if ($err !== false) {
              array_push($keywordErrors, $err);
          }
        }
        if (!empty($keyword2)) {
          $err = trfProcesskeyword($keyword2);
          if ($err !== false) {
              array_push($keywordErrors, $err);
          }
        }
        if (!empty($keyword3)) {
          $err = trfProcesskeyword($keyword3);
          if ($err !== false) {
              array_push($keywordErrors, $err);
          }
        }

        $keywordErrors = array_unique($keywordErrors);
        if (count($keywordErrors) > 0) {
            echo implode("\n\n<br />", $keywordErrors);
            return;
        }
      }

      if (pagesWithTalkingAbout($keyword1, $keyword2, $keyword3) < 100) {
        echo "Your Facebook keywords did not find enough potential traffic, please try adding more keywords and/or try replacing your current ones with more general (less specific) keywords.<br/><br/>\n";
      }
      else {
        if ($twitterReady) {
          if (!empty($keyword1)) {
            trfSearchTweets($keyword1);
          }
          if (!empty($keyword2)) {
            trfSearchTweets($keyword2);
          }
          if (!empty($keyword3)) {
            trfSearchTweets($keyword3);
          }
        }

        if ($twitterReady && tweetsCount($keyword1, $keyword2, $keyword3) < 100) {
          echo "Your Twitter keywords did not find enough potential traffic, please try adding more keywords and/or try replacing your current ones with more general (less specific) keywords.<br/><br/>\n";
        }
        else {
          if ($facebookReady) {
            if (!trfCheckHistory('facebook', $id)) {
              createFacebookPost($id);
              echo "Created your Facebook post and getting Facebook traffic for you now!<br/><br/>\n";
            } else {
              echo "Facebook Post already exists. Skipping...<br/><br/>\n";
            }
          }

          if ($twitterReady) {
            if (!trfCheckHistory('twitter', $id)) {
              createTweet($id, $keyword1, $keyword2, $keyword3);
              echo "Created your Twitter post and getting Twitter traffic for you now!<br/><br/>\n";
            } else {
              echo "Twitter Post already exists. Skipping...<br/><br/>\n";
            }
          }

          if ($redditReady) {
            if (!trfCheckHistory('reddit', $id)) {
              createRedditPost($id);
              echo "Created your Reddit post and getting Reddit traffic for you now!<br/><br/>\n";
            } else {
              echo "Reddit Post already exists. Skipping...<br/><br/>\n";
            }
          }
        }
      }
    }

    function pagesWithTalkingAbout($keyword1, $keyword2, $keyword3) {
      global $wpdb;

      $tbl_kws_fb = $wpdb->prefix . "trfpages";
      $row = $wpdb->get_row("SELECT count(id) as ct FROM $tbl_kws_fb WHERE talking_about >= 100 and keyword in ('$keyword1', '$keyword2', '$keyword3')");

      return intval($row->ct);
    }

    function tweetsCount($keyword1, $keyword2, $keyword3) {
      global $wpdb;

      $tbl_kws_fb = $wpdb->prefix . "trftweets";
      $row = $wpdb->get_row("SELECT count(id) as ct FROM $tbl_kws_fb WHERE keyword in ('$keyword1', '$keyword2', '$keyword3')");

      return intval($row->ct);
    }
