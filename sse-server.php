<?php
define( 'WP_USE_THEMES', false );
require( '../../../wp-load.php' );

header("Content-Type: text/event-stream\n\n");

$id = $_GET['id'];
$get_traffic = get_post_meta($id, 'trf_get_traffic', true);

if ($get_traffic == '1') {
    update_post_meta($id, 'trf_get_traffic', 0);
    sendEvent('finish', 'Succssfully Turned off');
    exit;
}

update_post_meta($id, 'trf_get_traffic', 1);

$facebookReady    = !empty(get_post_meta(111111113, 'trfFbAccessToken', TRUE));
$twitterReady     = !empty(get_post_meta(111111113, 'trfTwTokenSecret', TRUE));
$redditReady      = !empty(get_post_meta(111111113, 'trfRdRefreshToken', TRUE));

$keyword1         = get_post_meta($id, 'trf_keyword1', true);
$keyword2         = get_post_meta($id, 'trf_keyword2', true);
$keyword3         = get_post_meta($id, 'trf_keyword3', true);

if ($facebookReady) {
    $keywordErrors = array();
    if (!empty($keyword1)) {
        sendEvent('ping', "Analyzing keyword \"$keyword1\" and setting up your Facebook traffic campaign...");
        $err = trfProcesskeyword($keyword1);
        if ($err !== false) {
            array_push($keywordErrors, $err);
        }
    }
    if (!empty($keyword2)) {
        sendEvent('ping', "Analyzing keyword \"$keyword2\" and setting up your Facebook traffic campaign...");
        $err = trfProcesskeyword($keyword2);
        if ($err !== false) {
            array_push($keywordErrors, $err);
        }
    }
    if (!empty($keyword3)) {
        sendEvent('ping', "Analyzing keyword \"$keyword3\" and setting up your Facebook traffic campaign...");
        $err = trfProcesskeyword($keyword3);
        if ($err !== false) {
            array_push($keywordErrors, $err);
        }
    }

    $keywordErrors = array_unique($keywordErrors);
    if (count($keywordErrors) > 0) {
        sendEvent('error', implode("\n", $keywordErrors));
        return;
    }
}

if (pagesWithTalkingAbout($keyword1, $keyword2, $keyword3) < 100) {
    sendEvent('error', "Your Facebook keywords did not find enough potential traffic, please try adding more keywords and/or try replacing your current ones with more general (less specific) keywords");
}
else {
    if ($twitterReady) {
        if (!empty($keyword1)) {
            sendEvent('ping', "Analyzing keyword \"$keyword1\" and setting up your Twitter traffic campaign");
            trfSearchTweets($keyword1);
        }
        if (!empty($keyword2)) {
            sendEvent('ping', "Analyzing keyword \"$keyword2\" and setting up your Twitter traffic campaign");
            trfSearchTweets($keyword2);
        }
        if (!empty($keyword3)) {
            sendEvent('ping', "Analyzing keyword \"$keyword3\" and setting up your Twitter traffic campaign");
            trfSearchTweets($keyword3);
        }
    }
    if ($twitterReady && tweetsCount($keyword1, $keyword2, $keyword3) < 100) {
        sendEvent('error', "Your Twitter keywords did not find enough potential traffic, please try adding more keywords and/or try replacing your current ones with more general (less specific) keywords");
    }
    else {
        if ($facebookReady) {
            if (!trfCheckHistory('facebook', $id)) {
                createFacebookPost($id);
                sendEvent('ping', "Created your Facebook post and getting Facebook traffic for you now!");
            } else {
                sendEvent('ping', "Facebook Post already exists. Skipping...");
            }
        }
        if ($twitterReady) {
            if (!trfCheckHistory('twitter', $id)) {
                createTweet($id, $keyword1, $keyword2, $keyword3);
                sendEvent('ping', "Created your Twitter post and getting Twitter traffic for you now!");
            } else {
                sendEvent('ping', "Twitter Post already exists. Skipping...");
            }
        }
        if ($redditReady) {
            if (!trfCheckHistory('reddit', $id)) {
                createRedditPost($id);
                sendEvent('ping', "Created your Reddit post and getting Reddit traffic for you now!");
            } else {
                sendEvent('ping', "Reddit Post already exists. Skipping...");
            }
        }
    }
}

sendEvent('finish', 'Completed.');

function pagesWithTalkingAbout($keyword1, $keyword2, $keyword3) {
    global $wpdb;

    $tbl_kws_fb = $wpdb->prefix . "trfpages";
    $row = $wpdb->get_row("SELECT count(id) as ct FROM $tbl_kws_fb WHERE talking_about >= 50 and keyword in ('$keyword1', '$keyword2', '$keyword3')");

    return intval($row->ct);
}

function tweetsCount($keyword1, $keyword2, $keyword3) {
    global $wpdb;

    $tbl_kws_fb = $wpdb->prefix . "trftweets";
    $row = $wpdb->get_row("SELECT count(id) as ct FROM $tbl_kws_fb WHERE keyword in ('$keyword1', '$keyword2', '$keyword3')");

    return intval($row->ct);
}

function sendEvent($type, $msg) {
    echo "event: $type\n";
    echo "data: $msg\n\n";
    ob_end_flush();
    flush();
}