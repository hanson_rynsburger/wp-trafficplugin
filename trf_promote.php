<?php
global $wpdb;

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Trf_Promote extends WP_List_Table {
    function __construct() {
        parent::__construct( array(
            'singular'=> 'promote',
            'plural' => 'promotes',
            'ajax'  => false
        ) );

        add_action( 'admin_head', array( &$this, 'admin_header' ) );
    }

    function extra_tablenav($which) {
        if ($which == "top") { }
        if ($which == "bottom") { }
    }

    function column_default($item, $column_name) {
        return $item[$column_name];
    }

    function column_action($item) {
        $id = $item['id'];
        return "<input type='submit' value='Save' class='button button-primary trf-save' data-id='{$id}' data-nonce='" . wp_create_nonce('trf_'.$id) . "' />";
    }

    function column_date($item) {
        $date = $item['date'];
        return date('Y-m-d', strtotime($date));
    }

    function column_title($item) {
        $id = $item['id'];
        $title = $item['title'];
        return "<a href='" . get_edit_post_link($id) . "'>{$title}</a>";
    }

    function column_traffic_on($item) {
        $id = $item['id'];
        $traffic_on = $item['traffic_on'];
        return "<input class='widefat trf-get-traffic' type='checkbox' name='trf-get-traffic' value='1' " . ($traffic_on ? 'checked' : '') . " />";
    }

    function column_keyword1($item) {
        $keyword1 = $item['keyword1'];
        return "<input type='text' value='{$keyword1}' name='keyword1' />";
    }
    function column_keyword2($item) {
        $keyword2 = $item['keyword2'];
        return "<input type='text' value='{$keyword2}' name='keyword2' />";
    }
    function column_keyword3($item) {
        $keyword3 = $item['keyword3'];
        return "<input type='text' value='{$keyword3}' name='keyword3' />";
    }

    function get_columns() {
        return $columns= array(
            'title'         => 'Title',
            'date'          => 'Date',
            'type'          => 'Type',
            'keyword1'      => 'Keyword1',
            'keyword2'      => 'Keyword2',
            'keyword3'      => 'Keyword3',
            'action'        => 'Action',
            'traffic_on'    => 'Get Traffic'
        );
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'title'         => array('title', false),
            'date'          => array('date', false),
            'type'          => array('type', false),
            'keyword1'      => array('keyword1', false),
            'keyword2'      => array('keyword2', false),
            'keyword3'      => array('keyword3', false),
            'traffic_on'    => array('traffic_on', false)
        );
        return $sortable_columns;
    }

    function prepare_items() {
        $per_page = 50;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();

        $data = [];

        $post_types = array();
        if (empty($_POST['checker']) || isset($_POST['show_pages'])) {
            array_push($post_types, 'page');
        }
        if (empty($_POST['checker']) || isset($_POST['show_posts'])) {
            array_push($post_types, 'post');
        }
        if (empty($_POST['checker']) || isset($_POST['show_products'])) {
            array_push($post_types, 'product');
        }
        if (empty($_POST['checker']) || isset($_POST['show_articles'])) {
            array_push($post_types, 'article');
        }

        if (count($post_types) > 0) {
            $args = array(
                "post_type" => $post_types,
                "posts_per_page" => $per_page,
                "post_status" => array("publish"),
                "offset" => $per_page * ($this->get_pagenum()-1),
                "order" => (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc'
            );

            if (!empty($_REQUEST['orderby'])) {
                if ($_REQUEST['orderby'] == 'id') {
                    $args['orderby'] = 'id';
                } else if ($_REQUEST['orderby'] == 'title') {
                    $args['orderby'] = 'title';
                } else if ($_REQUEST['orderby'] == 'date') {
                    $args['orderby'] = 'date';
                } else if ($_REQUEST['orderby'] == 'type') {
                    $args['orderby'] = 'type';
                } else if ($_REQUEST['orderby'] == 'keyword1') {
                    $args['orderby'] = 'meta_value';
                    $args['meta_key'] = 'trf_keyword1';
                } else if ($_REQUEST['orderby'] == 'keyword2') {
                    $args['orderby'] = 'meta_value';
                    $args['meta_key'] = 'trf_keyword2';
                } else if ($_REQUEST['orderby'] == 'keyword3') {
                    $args['orderby'] = 'meta_value';
                    $args['meta_key'] = 'trf_keyword3';
                } else if ($_REQUEST['orderby'] == 'traffic_on') {
                    $args['orderby'] = 'meta_value';
                    $args['meta_key'] = 'trf_get_traffic';
                }
            }

            $query1 = new WP_Query($args);

            if ( $query1->have_posts() ) {
                // The Loop
                while ( $query1->have_posts() ) {
                    $query1->the_post();
                    $p = get_post();

                    array_push($data, array(
                        "id" => $p->ID,
                        "title" => $p->post_title,
                        "date" => $p->post_date,
                        "type" => ucfirst($p->post_type),
                        "keyword1" => get_post_meta($p->ID, 'trf_keyword1', true),
                        "keyword2" => get_post_meta($p->ID, 'trf_keyword2', true),
                        "keyword3" => get_post_meta($p->ID, 'trf_keyword3', true),
                        "traffic_on" => get_post_meta($p->ID, 'trf_get_traffic', true) == 1
                    ));
                }
                wp_reset_postdata();
            }
        }
        $current_page = $this->get_pagenum();

        $this->items = $data;
        $this->set_pagination_args(array(
            'total_items' => $query1->found_posts,      // WE have to calculate the total number of items
            'per_page'    => $per_page,                 // WE have to determine how many items to show on a page
            'total_pages' => $query1->max_num_pages     // WE have to calculate the total number of pages
        ));
    }
}

function trf_promote() {
    echo '<div class = "wrap">
        <div class = "fbvahead">' . TRAFFIC_PLUGIN_LOGO . ' </div>
        <h1>Promote</h1>
        <hr />';

    if (isset($_POST['checker'])) {
        update_post_meta(111111113, 'trf_show_posts', isset($_POST['show_posts']));
    }
    if (isset($_POST['checker'])) {
        update_post_meta(111111113, 'trf_show_pages', isset($_POST['show_pages']));
    }
    if (isset($_POST['checker'])) {
        update_post_meta(111111113, 'trf_show_products', isset($_POST['show_products']));
    }
    if (isset($_POST['checker'])) {
        update_post_meta(111111113, 'trf_show_articles', isset($_POST['show_articles']));
    }

    $show_posts = get_post_meta(111111113, 'trf_show_posts', true) ? 'checked="checked"' : '';
    $show_pages = get_post_meta(111111113, 'trf_show_pages', true) ? 'checked="checked"' : '';
    $show_products = get_post_meta(111111113, 'trf_show_products', true) ? 'checked="checked"' : '';
    $show_articles = get_post_meta(111111113, 'trf_show_articles', true) ? 'checked="checked"' : '';

    echo '<form id="filterdata" method="post" action=""><input type="hidden" name="checker" value="1" />';
    echo 'Show &nbsp;&nbsp;<label><input type="checkbox" value="1" name="show_posts" ' . $show_posts . ' onChange="submit()" /> Posts</label>
        &nbsp;&nbsp;<label><input type="checkbox" value="1" name="show_pages" ' . $show_pages . ' onChange="submit()" /> Pages</label>
        &nbsp;&nbsp;<label><input type="checkbox" value="1" name="show_products" ' . $show_products . ' onChange="submit()" /> Products</label>
        &nbsp;&nbsp;<label><input type="checkbox" value="1" name="show_articles" ' . $show_articles . ' onChange="submit()" /> Articles</label>';
    echo '</form>';

    echo '<form id="pagedata" method="post" action="">';
    $wp_list_table = new Trf_Promote();
    $wp_list_table->prepare_items();
    $wp_list_table->display();

    echo '</form>';

    $save_url = plugins_url( 'promote-save.php', __FILE__ );
    $sse_url = plugins_url( 'sse-server.php', __FILE__ );

    echo '
    <script type="text/javascript">
    jQuery(document).ready(function () {
        $ = jQuery;
        jQuery(".trf-get-traffic").toggleSwitch({ height: "28px" });
        jQuery(".trf-save").click(function(evt) {
            var self = $(this);
            var parent = $(self.closest("tr"));
            self.attr("disabled", "disabled");

            request = $.ajax({
                type: "post",
                url: "' . $save_url . '",
                data: {
                    id: self.data("id"),
                    trf_traffic_nonce: self.data("nonce"),
                    keyword1: parent.find("[name=keyword1]").val(),
                    keyword2: parent.find("[name=keyword2]").val(),
                    keyword3: parent.find("[name=keyword3]").val(),
                    keyword: "true"
                }
            })
            .done(function(msg) {
                toastr.success("Saved.")
            }).always(function() {
                self.removeAttr("disabled");
            });
            evt.preventDefault();
            return false;
        });

        jQuery(".trf-get-traffic").change(function(evt) {
            var self = $(evt.currentTarget);
            var parent = $(self.closest("tr"));
            parent.addClass("pending");
            var btn = parent.find(".trf-save");

            var evtSource;

            if (EventSource) {
                evtSource = new EventSource("'.$sse_url.'?id=" + btn.data("id"));

                evtSource.onopen = function() {
                    console.log("event source opened");
                };

                evtSource.onerror = function(e) {
                    console.log("EventSource failed.", e);
                };

                evtSource.addEventListener("ping", function(e) {
                    toastr.info(e.data);
                });

                evtSource.addEventListener("error", function(e) {
                    evtSource.close();
                    toastr.error(e.data);
                    parent.removeClass("pending");
                });

                evtSource.addEventListener("finish", function(e) {
                    evtSource.close();
                    toastr.success(e.data);
                    parent.removeClass("pending");
                });
            } else {
                console.log("Event Source is not available. using ajax...");
                toastr.info("Please wait while server processing the requests. It takes less than a minute");
                var request = $.ajax({
                    type: "post",
                    url: "' . $save_url . '",
                    data: {
                        id: btn.data("id"),
                        trf_traffic_nonce: btn.data("nonce"),
                        get_traffic: parent.find("[name=trf-get-traffic]").is(":checked") ? "1" : ""
                    }
                })
                .done(function(msg) {
                    if(msg) toastr.success(msg);
                }).always(function() {
                    parent.removeClass("pending");
                });
            }

            evt.preventDefault();
            return false;
        });
    });
    </script>';
}
