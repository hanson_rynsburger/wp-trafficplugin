<?php
function trf_install() {
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    if (is_plugin_active('trafficplugin_pro/trafficplugin.php')) {
        deactivate_plugins('trafficplugin_pro/trafficplugin.php');
    }

    global $wpdb;
    $wpdb->show_errors();

    $table_name = $wpdb->prefix ."trfpages";

    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id int(11) NOT NULL AUTO_INCREMENT,
            node_id varchar(30),
            title varchar(1024),
            talking_about int(11),
            likes int(11),
            keyword varchar(255) NOT NULL,
            UNIQUE KEY id (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

        dbDelta($sql);
    }

    $table_name = $wpdb->prefix ."trftweets";

    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id int(11) NOT NULL AUTO_INCREMENT,
            node_id varchar(30),
            tweet varchar(1024),
            keyword varchar(255) NOT NULL,
            UNIQUE KEY id (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "trfhistory";

    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id INTEGER(100) UNSIGNED AUTO_INCREMENT,
            post_id int(11),
            created int(11),
            source varchar(20),
            url varchar(1024),
            title varchar(255),
            raw_result text,
            UNIQUE KEY id (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "trfcommenthistory";

    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id INTEGER(100) UNSIGNED AUTO_INCREMENT,
            post_id int(11),
            history_date varchar(20),
            fb_post varchar(50),
            timesent int(11),
            UNIQUE KEY id (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "trfdailypost";

    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id INTEGER(100) UNSIGNED AUTO_INCREMENT,
            postdate varchar(20),
            postid int(11),
            UNIQUE KEY id (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

        dbDelta($sql);
    }

    $table_name = $wpdb->prefix ."trfcomments";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id INTEGER(100) UNSIGNED AUTO_INCREMENT,
            comment text,
            campaign text,
            UNIQUE KEY id (id)
        )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

        dbDelta($sql);

        $wpdb->insert( $table_name,  array(
            'comment' =>'{Brilliant |Really good}{||post|info|} {|thanks|thanks a lot} {love [KEYWORD]|{but|} {read|check out| check out this} {[LINK] | my page}}',
            'campaign' => 'ALL'
        ) );

        $wpdb->insert( $table_name,  array(
            'comment' =>'{Interesting|Cool } {[KEYWORD|}{post|info|} {|thanks|so much} {this is|} {really great|really good} {but look | take a look at } {this |this too | my fanpage } {[LINK] | } ',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
                'comment' =>'{Awesome|Nice|Top} {| [KEYWORD] post}  {but |} {read|check out| check out this} {[LINK] | our page} ',
                'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'Thanks {|!| for the post| for the info|} {big fan here|} {but |} {you need to see |you need to read} {this|} {[LINK] | page}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'Anyone else {seen [LINK] | thinks this [LINK] is cool | seen our page | thinks my page is cool } {?|}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{who else|who } {has seen|has read} {this|} {[LINK] | our {[KEYWORD]|} fanpage | page } {?|}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{Check this out |Take a look at | Read this} { too | as well} {guys |everyone} [LINK] {great [KEYWORD] stuff}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{Reminds me of this | Similiar ?} {[LINK]} {who agrees?|who else is a [KEYWORD] fan?}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'anyone {|else} {love| like } {|this | this post|[KEYWORD]} { [LINK] | on my page } as much as {me|i do}  ',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{loving the| love the| such a great}  {post | page | fanpage|[KEYWORD]} {but look | take a look at }{this |this too} {[LINK] | my page | our page too}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{Valuable Post | Important Post| Important Info} {on my {page|[KEYWORD]| [LINK]} {!|}}',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'Anything { about | related to} {this|[KEYWORD]} is {so|very|really|} {great|amazing}{you need to see |you need to read} {this| [LINK] | {our page | my page | my fanpage}  ',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'More {info|posts|stuff} {on [KEYWORD]|about [KEYWORD]|}{like this | such as this [LINK] } {please|ok?} {like if you agree|like = agree|who agrees?|}  ',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{Hey everyone|Hey | Guys |  You need to }{love [KEYWORD] ?|}{read|check out| check out this} { [LINK] |us too } ',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{Stuff |posts} {like this} {[LINK]|} are why {I love | I like | everyone loves | everyone likes} [KEYWORD]{agree ?|? |} ',
            'campaign' => 'ALL'
        ) );
        $wpdb->insert( $table_name,  array(
            'comment' =>'{<3| I <3 | Who else? <3| Who else? <3} {[KEYWORD]|[LINK]} {?|} ',
            'campaign' => 'ALL'
        ) );
    }

    update_post_meta(111111113, 'trf_show_posts', true );
    update_post_meta(111111113, 'trf_show_pages', true );
    update_post_meta(111111113, 'trf_show_products', true );
    update_post_meta(111111113, 'trf_show_articles', true );

    /*
    // this ensures that sorting by keyword1, 2, 3 displays empty one. but will generate fatal error with lots of posts
    $the_query = new WP_Query(
        array(
            "posts_per_page" => -1,
            "post_type" => array('post', 'page', 'product')

        )
    );

    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) : $the_query->the_post();
            $postmeta = get_post_meta(get_the_ID(), 'trf_keyword1', true);
            if (empty($postmeta)) {
                add_post_meta(get_the_ID(), 'trf_keyword1', '');
            }
            $postmeta = get_post_meta(get_the_ID(), 'trf_keyword2', true);
            if (empty($postmeta)) {
                add_post_meta(get_the_ID(), 'trf_keyword2', '');
            }
            $postmeta = get_post_meta(get_the_ID(), 'trf_keyword3', true);
            if (empty($postmeta)) {
                add_post_meta(get_the_ID(), 'trf_keyword3', '');
            }
            $postmeta = get_post_meta(get_the_ID(), 'trf_get_traffic', true);
            if (empty($postmeta)) {
                add_post_meta(get_the_ID(), 'trf_get_traffic', 0);
            }
        endwhile;
    }
    */
}
